# frozen_string_literal: true

class TransactionCreate
  def self.perform(transaction_params, user, transaction = nil)
    WalletTransaction.transaction do
      new(transaction_params, user, transaction).call
    end
  end

  def initialize(params, user, transaction)
    @params = params
    @user = user
    @transaction = transaction
  end

  def call
    transaction.save!
    transaction_validate
    add_users_and_wallets
    transaction
  end

  protected

  def transaction
    @transaction ||= Builders::TransactionBuilder.build do |builder|
      builder.build_initiator(@user.id)
      builder.build_type(@params[:type])
      builder.build_currency(wallet_to)
      builder.build_money(@params[:transfered_money],
                          @params[:tax_paid_by_the_recipient])
    end
  end

  def wallet_from
    return if @params[:wallet_from_id].blank?

    @wallet_from ||= @user.wallets.find(@params[:wallet_from_id])
  end

  def wallet_to
    return @wallet_to if @wallet_to.present?

    @wallet_to = if @params[:wallet_to].present?
                   Wallet.find_by(identity: @params[:wallet_to])
                 elsif @params[:wallet_to_id].present?
                   Wallet.find(@params[:wallet_to_id].to_i)
                 end
    raise StandardError, 'recipient wallet not found' if @wallet_to.blank?

    @wallet_to
  end

  def transaction_validate
    valid_wallets
    valid_currency
  end

  def valid_wallets
    return true if wallet_from.blank? || wallet_from.id != wallet_to.id

    transaction.update(status: :failed, error_message: 'invalid wallet')
    false
  end

  def valid_currency
    return if wallet_from.blank? || \
              wallet_from.currency_id == wallet_to.currency_id

    @transaction.update(status: :failed,
                        error_message: 'currencies is not equal')
  end

  def add_users_and_wallets
    add_wallets
    add_users
  end

  def add_wallets
    transaction.wallets << wallet_to
    transaction.update(wallet_to_id: wallet_to.id)
    return if wallet_from.blank? || !valid_wallets

    transaction.wallets << wallet_from
    transaction.update(wallet_from_id: wallet_from.id)
  end

  def add_users
    transaction.users << @user
    return if @wallet_to.admin_id == @user.id

    second_user = User.find(@wallet_to.admin_id)
    transaction.users << second_user
  end
end
