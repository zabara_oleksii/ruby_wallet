# frozen_string_literal: true

class ConfirmTransaction
  def self.perform(transfer)
    Wallet.transaction do
      new(transfer).call
    end
  end

  def initialize(transfer)
    @transfer = transfer
  end

  def call
    if @transfer.wallet_from_id.present?
      wallet_from = Wallet.find(@transfer.wallet_from_id)
      result_from = wallet_from.withdraw_money(@transfer, sum_for_withdraw)
      if result_from.status == :done
        @transfer.update(status: :done)
      else
        return @transfer.update(result_from.as_json)
      end
    end
    wallet_to = Wallet.find(@transfer.wallet_to_id)
    result_to = wallet_to.receiving_money(@transfer, sum_for_receiving)
    if result_to.status == :done
      @transfer.update(status: :done)
    else
      @transfer.update(result_to.as_json)
    end
  end

  private

  def sum_for_withdraw
    return @sum_for_withdraw if @sum_for_withdraw.present?

    @sum_for_withdraw = if @transfer.tax_paid_by_the_recipient
                          @transfer.transfered_money
                        else
                          @transfer.transfered_money + @transfer.tax
                        end
  end

  def sum_for_receiving
    return @sum_for_receiving if @sum_for_receiving.present?

    @sum_for_receiving = if @transfer.tax_paid_by_the_recipient
                           @transfer.transfered_money - @transfer.tax
                         else
                           @transfer.transfered_money
                         end
  end
end
