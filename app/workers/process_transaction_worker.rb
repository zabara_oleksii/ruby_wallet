# frozen_string_literal: true

class ProcessTransactionWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(transaction_id)
    transaction = WalletTransaction.find(transaction_id)
    ConfirmTransaction.perform(transaction)
  end
end
